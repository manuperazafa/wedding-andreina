
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en" itemscope itemtype="http://schema.org/Site" xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

<?php date_default_timezone_set('America/Caracas'); ?>
<?php 
    if($this->Session->read('token') == 'valido'){
            
    }
    else {
        header("Location: http://example.com/myOtherPage.php");
        exit();
    }
?>
<title>Web De Mi Boda</title>
<meta name="Title" content="Web De Mi Boda" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

<!--<meta property="fb:app_id" content="" />-->
<meta property="og:title" content="Web De Mi Boda"/>
<meta property="og:type" content="website"/>
<!--meta property="og:site_url" content=""/-->
<meta property="og:url" content=""/>
<meta property="og:image" content="img/web_de_mi_boda.jpg"/>
<meta property="og:site_name" content="Web De Mi Boda"/>


<meta itemprop="name" content="Web De Mi Boda"/>

<meta itemprop="image" content="img/web_de_mi_boda.jpg" />


<!-- ==================== CSS ==================== -->

<link rel="stylesheet" href="css/fonts.css?v=3" />
<link rel="stylesheet" href="css/bootstrap.css?v=3" />
<link rel="stylesheet" href="css/oceano.css?v=3" />
<link rel="icon" href="img/favicon.ico?v=3" type="image/x-icon" />

<!-- ==================== JS ==================== -->
<script type="text/javascript">
        var _gaq = _gaq || [], _DasGATrackPage, _DasGATrackEvent;

        (function() {
            var _DasGaAccountSetup = function(cb) {
                _gaq.push(['_setAccount', 'UA-54485521-1']);
                _gaq.push(['_setDomainName', 'webdemiboda.com']);
                cb();
            };

            _DasGATrackPage = function(trackTag) {
                _DasGaAccountSetup(function() {
                    _gaq.push(['_trackPageview', trackTag]);
                });
            };

            _DasGATrackEvent = function(eventData) {
                _DasGaAccountSetup(function() {
                    _gaq.push(['_trackEvent'].concat(eventData));
                });
            };

    
        })();

</script>

    <script type="text/javascript">
        (function() {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' === document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();
    </script>

<script type="text/javascript">
    if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
        var msViewportStyle = document.createElement("style");
        msViewportStyle.appendChild(
                document.createTextNode(
                "@-ms-viewport{width:auto!important}"
                )
                );
        document.getElementsByTagName("head")[0].
                appendChild(msViewportStyle);
    }
</script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libimg/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libimg/respond.jimg/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>
    <body>
        <div id="fb-root"></div>
        <div id="main-loading" class="loading main_loader">
            <span class="spinner"></span>
        </div>
        <div id="main-container" style="display: none;">
            
<div id="wrapper">
	
	<!-- Welcome message -->
	<section id="main" class="cont_welcome">
		<div class="navbar main_navigation" role="navigation">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand visible-xs text_white museo500" href="#">
						<span class="initials romance">K&amp;S</span>
						<span class="initials acuarela">M&amp;C</span>
						<span class="initials oceano">A&amp;A</span>
						<span class="initials vintage">L&amp;S</span>
						<span class="initials moderna">C&amp;J</span>
						<span class="initials clasica">S&amp;S</span>
					</a>
				</div>
				<div class="navbar-collapse collapse black">
					<ul class="nav navbar-nav row" id="menu_links">
						<li class="col-sm-2 col-xs-12">
							<a data-section-name="matrimonio" href="javascript:void(0);">Matrimonio</a>
						</li>
						<li class="col-sm-2 col-xs-12">
							<a data-section-name="nosotros" href="javascript:void(0);">Nosotros</a>
						</li>
						<li class="col-sm-1 col-xs-12">
							<a data-section-name="album" href="javascript:void(0);">Álbum</a>
						</li>
						<li class="col-sm-2 col-xs-12">
							<a data-section-name="informacion" href="javascript:void(0);">Información</a>
						</li>
						<li class="col-sm-2 col-xs-12">
							<a data-section-name="lugar" href="javascript:void(0);">Lugar</a>
						</li>
						<li class="col-sm-2 col-xs-12">
							<a data-section-name="mensajes" href="javascript:void(0);">Mensajes</a>
						</li>
						<li class="col-sm-1 col-xs-12">
							<a data-section-name="rsvp" href="javascript:void(0);">Rsvp</a>
						</li>
					</ul>
				</div>
				<!--/.nav-collapse -->
				
			</div>
			<!--/.container-fluid -->
		</div>
		
		<div class="vertical_align_1_2">
			<div class="vertical_align_2_2">
				<div  class="cont_img">
					<div id="AFTexto">Alfredo + Andreina</div>
				</div>
			</div>
		</div>
	</section>
	
	<!-- main navigation -->
	
	<!-- About the Wedding section -->
	<section id="matrimonio" class="">
		<div class="container cont_wedding">
	       <div class="row">
                <div class="col-sm-3">
                </div>
                <div class="col-sm-6">
                    <h2 class="text-center upper"><strong>Nuestro Matrimonio</strong></h2>
                    <p class="text-center">Nuestra Boda será el 17 de diciembre de 2016 y nada nos complacería más que pudiesen compartir este día tan especial con nosotros.<br> 
    				LOS ESPERAMOS!!!</p>
                    <p class="text-center">
    				    <a  href="https://goo.gl/maps/yjTNFF77b2H2" target="_blank" class="upper">
    					   <span class="glyphicon glyphicon-map-marker"></span>
    					   Ver mapa
    				    </a>
                    </p>
                    <div class="col-sm-3"></div>
                </div>
            </div>
        </div>	 
    </section>
	<div class="parallax_1 oceano low" data-stellar-background-ratio="0.5" data-img-src="img/oceano_high.jpg?v=3" >
				<div class="love_story">
			<img data-section="love_story" data-template="oceano" src="img/aboutus_oceano.png?v=3" alt="Nuestra historia de amor">
			<!--romance-->
		</div>
			</div>
	<!-- About Us section -->
	<section id="nosotros" class="white cont_aboutus">
		<div class="container">
            <div class="title">
                <i class="ocational_bs1"></i>
                <h2 class="upper white">Nosotros</h2>
                <i class="ocational_bs2"></i>
            </div>
        	<div class="row">
        		<div class="col-sm-6">
        			<h2 class="upper">
                        <span class="names oceano">Andreina</span>                
                        <span class="heart"></span></h2>
        			<p>Definitivamente el amor llega cuando uno menos lo espera y en las circunstancias menos pensada. Así llegó
                        <span class="names oceano">Alfredo</span>a mi vida, un diciembre en el trabajo, una amiga muy querida para mí me lo presentó, recuerdo que todo empezó como una broma que finalmente ambos decidimos tomarlo en serio, sólo me resta decir que ese diciembre el Niño Jesús me trajo uno de los regalos más lindos, el amor verdadero, esos que te regalan paz, seguridad y tranquilidad a tu corazón, y eso es Él para mí, Mi Amor!</p>
        		</div>
        	 	<div class="col-sm-6">
        	 		<h2 class="upper">
                        <span class="names oceano">Alfredo</span>
                        <span class="heart second"></span></h2>
        			<p>   
                        Todo empezo un día del mes de septiembre, cuando vi a una hermosa mujer de aproximadamente un metro setenta, de bellas curvas y con una sonrisa encantadora, pasando por los pasillos del que es nuestro lugar de trabajo. Por mi cabeza rondaban ideas de "¿será casada?", "¿tendrá hijos?" o "¿tendrá novio?", igual eso no me atormentaba, lo que me atormentaba era el hecho de no conocer a esa hermosa mujer y ver como pasaba por mi puesto de trabajo. Gracias a una pequeña reunión logré conocer a la mujer de mis sueños, verla sonreir y mirarla a los ojos era casi como sentir un efecto de relajación. Las citas con <span class="names oceano">Andreina</span> eran cosa de otro mundo, compartimos risas, miradas y gustos. Decidido una noche de febrero le propuse que fuera mi novia  y bueno aquí estamos.
                    </p>
        	 	</div>
        	</div>
        </div>	
    </section>
	
	<!-- Gallery section -->
	<section id="album" class="height500 pink cont_gallery">
		<div class="container">
            <span class="glyphicon glyphicon-camera"></span>
            <h2 class="upper text_white"><strong>Fotos</strong></h2>

            <div>

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                                <div id="grid" class="img_container" style="position:relative;">

                                    
                                    
                                    <div data-photo="3" class="picture-item col-sm-3 col-xs-6" data-groups='["lugar"]' data-date-created="2010-09-14" data-title="">
                                        <a href="javascript:void(0);"><img data-img-src="img/a3.JPG" src="img/a3.JPG" /></a>
                                    </div>
                                    
                                    <div data-photo="4" class="picture-item col-sm-3 col-xs-6" data-groups='["matrimonio"]' data-date-created="2010-09-14" data-title="">
                                        <a href="javascript:void(0);"><img data-img-src="img/a4.JPG" src="img/a4.JPG" /></a>
                                    </div>
                                    
                                    <div data-photo="5" class="picture-item col-sm-3 col-xs-6" data-groups='["fiesta"]' data-date-created="2010-09-14" data-title="">
                                        <a href="javascript:void(0);"><img data-img-src="img/a5.JPG" src="img/a5.JPG" /></a>
                                    </div>
                                    
                                    <div data-photo="6" class="picture-item col-sm-3 col-xs-6" data-groups='["historia"]' data-date-created="2010-09-14" data-title="">
                                        <a href="javascript:void(0);"><img data-img-src="img/a6.JPG" src="img/a6.JPG" /></a>
                                    </div>   


                                </div>                                                                     
                            </div>
                         </div>
                    </div>
                 </div>
            </div>	
        </div>
    </section>
	
	<!-- General information section -->
	<section id="informacion" class="cont_informacion">
		
<div class="container">
	<div class="title">
		<i class="ocational_bs1"></i>
		<h2 class="upper white">Información</h2>
		<i class="ocational_bs2"></i>
	</div>
	<div class="heart"></div>
	<div class="row">
		<div class="col-sm-4">
			<h2 id="isla">Posadas y Hoteles</h2>
			
            <div class="hotel">
                <h4><strong>Buganvillas</strong></h4>
			     <p> Sector de Guarame<br/>
                 Contacto: Luis Poleo <br>
                 Tlf: 0414-011-20-54 <br>
				    <a href="http://buganvillasmargarita.com/" target="_blank">Ver página web </a>
			     </p>
            </div>
            
            <div class="hotel">
                <h4><strong>Posada Séptimo Cielo</strong></h4>
                 <p> Sector de Guarame<br/>
                 Contacto: Yskandar Takchi <br>
                 Tlf: 0416-243-0185 / 0295-242-63-33 <br>
                    <a href="http://www.posadaseptimocielo.com/" target="_blank">Ver página web </a>
                 </p>
            </div>

            <div class="hotel">
                <h4><strong>Maloka Hotel Boutique</strong></h4>
                 <p>Playa Guacuco - Isla de Margarita<br/>
                 Contacto: ventas@hotelboutiquemaloka.com<br>
                 Tlf: 0295-417-95-82 / 0295-263-20-65<br>
                    <a href="http://www.hotelboutiquemaloka.com" target="_blank">Ver página web </a>
                 </p>
            </div>
		
            <div class="hotel">
                <h4><strong>Villa Maloka</strong></h4>
                 <p>Playa Guacuco - Isla de Margarita<br/>
                 Contacto: Caridad López<br>
                 Tlf: 0414-789-44-73<br>
                    <a href="http://www.villamaloka.com" target="_blank">Ver página web </a>
                 </p>
            </div>

        </div>

		<div class="col-sm-4">
			
			<h2 id="isla">Salones de Belleza</h2>
                <div class="salon">
                    <h4><strong>Sthella Maquillaje Profesional</strong></h4>
                     <p>Tlf: (0295) 417-1797 / 0424-890-0362 <br>
                        Instagram: @sthellamaquillaje <br>
                        Locación: Av. Aldonza Manrique con Jovito Villalba, Urb. Playa el Angel. 
                     </p>
                </div>

                <div class="salon">
                    <h4><strong>Camerino </strong></h4>
                     <p>Teléfono: (0295) 500-2537 <br>
                        Web: www.camerinomaquillaje.com <br>
                        Instagram: @camerinomakeup <br>
                        Locación: Centro Comercial Parque Costa Azul 
                     </p>
                </div>

                <div class="salon">
                    <h4><strong>Grupo Samuel Torres</strong></h4>
                     <p>Teléfono: 0414-794-4557 <br>
                        Instagram: @stgrupoestilistas <br>
                        Locación: Consultar el traslado a un hotel
                     </p>
                </div>
		</div>


		<div class="col-sm-4">
			<h2 id="isla">Codigo de Vestimenta</h2>
			<h4><strong>Etiqueta Tropical</strong></h4>
            <h4 id="ellas"><strong>ELLAS</strong></h4>
            <p>Vestidos largos o semi largos. <br>
            </p>

            <h4 id="ellos"><strong>ELLOS</strong></h4>
            <p>Camisa de lino o guayabera manga larga o tres cuartos
            </p>

            <h4><strong>En General...</strong></h4>
            <p>Tonalidades Pasteles <br>
                Evitar blanco, negro y colores fuertes o llamativos</p>
		</div>
	</div>
</div>
	</section>
	
	<!-- Gifts section -->
	<section id="lugar" class="gift white">
		<div class="container">
	<div class="title">
		<i class="ocational_bs1"></i><h2 class="upper white">Lugar</h2><i class="ocational_bs2"></i>
	</div>
	<div class="row">
		<div class="col-sm-4">
			<h3>La Chulinga Margarita</h3>
            <p>
              Via playa guacuco <br/>
              Sector guarame<br/>
              <a href="https://goo.gl/maps/2RLpENmP6PC2l" target="_blank">Ver mapa</a> 
            </p>
            
		</div>
	 	<div class="col-sm-4">
	 		<div class="cont_img">
	 			<img data-section="gifts" data-template="oceano" src="img/gifts_oceano.png?v=3" alt="para: Katy & Salomón">
                <!--img data-section="gifts" data-template="acuarela" src="app/imageimg/gifts_acuarela.png" alt="para: Katy & Salomón">
                <img data-section="gifts" data-template="oceano" src="app/imageimg/gifts_oceano.png" alt="para: Katy & Salomón">
                <img data-section="gifts" data-template="vintage" src="app/imageimg/gifts_vintage.png" alt="para: Katy & Salomón">
                <img data-section="gifts" data-template="moderna" src="app/imageimg/gifts_moderna.png" alt="para: Katy & Salomón">
                <img data-section="gifts" data-template="clasica" src="app/imageimg/gifts_clasica.png" alt="para: Katy & Salomón"-->
	 		</div>
	 	</div>
	 	<div class="col-sm-4">
	 		<h3>Isla de Margarita</h3>
            <p>
                
                Basílica Nuestra Señora del Valle<br/>
                 
            </p>
	 	</div>
	</div>
</div>	</section>
	
	<!-- Message form & display section -->
	<section id="mensajes" class="white_bed_sheet">
		
<div class="container">
    <!--h2 class="upper">Mensajes</h2-->
    <div class="row">

        <div class="col-sm-4">
            <div class="form_box white">
                <div class="form_title">
                    <h2>Deje su mensaje</h2>
                </div>
                <div class="deco_img">
                    
                    <img data-section="messages" data-template="oceano" src="img/form_deco_oceano.png?v=3" alt="para: Katy & Salomón">
                    <!--img data-section="messages" data-template="acuarela" src="app/imageimg/form_deco_acuarela.png" alt="para: Katy & Salomón">
                    <img data-section="messages" data-template="oceano" src="app/imageimg/form_deco_oceano.png" alt="para: Katy & Salomón">
                    <img data-section="messages" data-template="vintage" src="app/imageimg/form_deco_vintage.png" alt="para: Katy & Salomón">
                    <img data-section="messages" data-template="moderna" src="app/imageimg/form_deco_moderna.png" alt="para: Katy & Salomón">
                    <img data-section="messages" data-template="clasica" src="app/imageimg/form_deco_clasica.png" alt="para: Katy & Salomón"-->
                    
                    
                </div>
                <div id="message_display" class="alert" style="display:none;"><span class="alert_icon">Alert</span></div>
                <form id="mensajes_form" class="to_happycouple">
                    <fieldset>
                        <input type="text" id="tbl_contact_name" class="form-control placehold" name="message_name" placeholder="DE:"  class="placehold" required>
                    </fieldset>
                    
                    <!--fieldset>
                            <input type="email" class="form-control placehold" placeholder="EMAIL" required>
                    </fieldset-->
                    <fieldset>
                        <textarea id="tbl_contact_message" class="form-control placehold" rows="7" name="message_message"  placeholder="MENSAJE:" class="placehold" required></textarea>
                    </fieldset>
                    <button  onclick="return false;" id="enviar" class="btn pull-right">
                        <span id="loading_btn" class="loading" style="display:none;"></span><!--loading-->
                        ENVIAR<span class="glyphicon glyphicon-play"></span></button>
                </form>
            </div>
        </div>

        <div class="col-sm-8">
            <div class="message_box overflow">
                <h2 class="upper">Buzón de felicitaciones</h2>
                <div id="messages_box" class="msg_box">
                    <ul class="each" id="mensajitos">
                                                   
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
	</section>
	
	<!-- RSVP section -->
	<section id="rsvp" class="pink cont_rsvp">
		
<div class="container">
	<div class="row">
		<div class="col-sm-4">
			<div class="cont_img">
				<!--<img src="" alt="RSVP">-->
			</div>
		</div>
		<div class="col-sm-8">
			<p>Por favor confirmar antes del 01 del noviembre 2016.<br />
				<strong>Los esperamos!</strong></p>
			     <form id="mensajes_form" class="to_happycouple">
                <button  onclick="return false;" id="confirmar" class="btn pull-right">
                        <span id="loading_btn" class="loading" style="display:none;"></span><!--loading-->
                        CONFIRMAR ASISTENCIA<span class="glyphicon glyphicon-play"></span></button>
			     </form>
		</div>
	</div>
</div>
	</section>
	<footer class="cont_footer black">
    </footer></div>
<div id="layer_map" class="cont_layer" style="display:none;">
	<div class="vertical_align_1_2">
    <div class="vertical_align_2_2">
        <div class="cont_map">
            <a id="close_map" href="javascript:void(0);" class="exit"><span class="glyphicon glyphicon-remove-circle"></span></a>
            <div id="map-canvas" style="width: 100%; height: 100%"></div>
        </div>
    </div>
</div></div>
<div id="layer_photos" class="cont_layer" style="display:none;">
	<div class="vertical_align_1_2">
    <div class="vertical_align_2_2">
        <div class="cont_img">
            <span id="photo_load" class="loader"></span>
            <a id="close_photos" href="javascript:void(0);" class="exit"><span class="glyphicon glyphicon-remove-circle"></span></a>
            <div id="slides">
                <!--<img data-src=".sl.png" alt="Photo1">-->
                <img data-img-src="img/big_thumb.png?v=3" src="img/big_thumb.png?v=3" alt="placeholder">
            </div>            
            <a id="photo_left" href="javascript:void(0);" class="pull_left prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
            <a id="photo_right" href="javascript:void(0);" class="pull_right next"><span class="glyphicon glyphicon-chevron-right"></span></a>

        </div>
    </div>
</div></div>
        </div>

<!--<script type="text/javascript" src="img/jimg/main.js?v=3"></script> -->
        <script type="text/javascript" src="js/modernizr.min.js?v=3"></script>
        <script type="text/javascript" src="js/detectizr.min.js?v=3"></script>
        <script type="text/javascript" src="js/lodash.compat.min.js?v=3"></script> 
        <script type="text/javascript" src="js/jquery-1.8.3.min.js?v=3"></script>
        <script type="text/javascript" src="js/jquery.scrollTo-min.js?v=3"></script>
        <script type="text/javascript" src="js/jquery.history.min.js?v=3"></script>
        <script type="text/javascript" src="js/jquery.shuffle.min.js?v=3"></script>   
        <!--<script type="text/javascript" src="/app_lib/jimg/jimg/utilimg/jquery/jquery.infinitescroll.js?v=3"></script>-->          
        <!--<script type="text/javascript" src="/app_lib/jimg/jimg/utilimg/underscore-min.js?v=3"></script>-->
        <script type="text/javascript" src="js/das-core.js?v=3"></script>
        <script type="text/javascript" src="js/das-formutils.js?v=3"></script>
        <!--<script type="text/javascript" src="/app_lib/jimg/jimg/utilimg/jquery/jquery.validate.min.js?v=3"></script>-->  
        <!--<script type="text/javascript" src="/app_lib/jimg/jimg/utilimg/jquery/jquery.slides.min.js?v=3"></script>-->         
        <script type="text/javascript" src="js/bootstrap.min.js?v=3"></script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script type="text/javascript" src="js/das-site-util.js?v=3"></script>
        <script type="text/javascript" src="js/template-site.js?v=3"></script> 
        <script type="text/javascript">
            $( document ).ready(function() {
    

              $("#enviar").click(function() {
                  
                  $.post( "EmailQueues/guardarMensaje", { nombre : $('#tbl_contact_name').val(), mensaje : $('#tbl_contact_message').val() })
                  //$.post("http://weddingnanycarlose.com.ve/EmailQueues/confirmoToken", { Eventorsvp: $('#Eventorsvp').val()})
                  .done(function( data ) {
                        alert('Enviado con exito');
                     });
              });

              $("#confirmar").click(function(){
                    $.getJSON( "EmailQueues/confirmoToken", function( data ) {
                      
                      
                      alert('Has confirmado con exito');

                      
                    });
              });

                  $.getJSON( "EmailQueues/obtenerMensajes", function( data ) {
                      
                      
                       $.each(data, function(key , value) {
                            
                            
                            $("#mensajitos").append('<li><span class="heart"></span>'+'<p>'+value.Mensaje.nombre+'</p>'+'<p>'+value.Mensaje.mensaje+'</p></li>');
                            
                            
                            

                        });

                      
                 });


            });
        </script>

    </body>
</html>
