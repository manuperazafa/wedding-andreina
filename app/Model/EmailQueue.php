<?php
App::uses('AppModel', 'Model');
/**
 * EmailQueue Model
 *
 */
class EmailQueue extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'email_queue';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'to';

}
