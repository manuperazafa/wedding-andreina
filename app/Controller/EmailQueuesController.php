<?php
App::uses('AppController', 'Controller');
/**
 * EmailQueues Controller
 *
 * @property EmailQueue $EmailQueue
 * @property PaginatorComponent $Paginator
 */
class EmailQueuesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public $uses = array('EmailQueue' , 'Email' , 'Mensaje');

/**
 * index method
 *
 * @return void
 */	
	
	function beforeFilter(){

		$this->layout = 'default2';
	}
	public function index() {
		$this->EmailQueue->recursive = 0;
		$this->set('emailQueues', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->EmailQueue->exists($id)) {
			throw new NotFoundException(__('Invalid email queue'));
		}
		$options = array('conditions' => array('EmailQueue.' . $this->EmailQueue->primaryKey => $id));
		$this->set('emailQueue', $this->EmailQueue->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = 'default2';
		if ($this->request->is('post')) {
			$this->EmailQueue->create();
			if ($this->EmailQueue->save($this->request->data)) {
				$this->Session->setFlash(__('The email queue has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The email queue could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->EmailQueue->exists($id)) {
			throw new NotFoundException(__('Invalid email queue'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->EmailQueue->save($this->request->data)) {
				$this->Session->setFlash(__('The email queue has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The email queue could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('EmailQueue.' . $this->EmailQueue->primaryKey => $id));
			$this->request->data = $this->EmailQueue->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->EmailQueue->id = $id;
		if (!$this->EmailQueue->exists()) {
			throw new NotFoundException(__('Invalid email queue'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->EmailQueue->delete()) {
			$this->Session->setFlash(__('The email queue has been deleted.'));
		} else {
			$this->Session->setFlash(__('The email queue could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function verificoToken($token = null){
		$this->autoRender = false;
		
		if($token){
			$stoken = $this->Email->find('first' , array('conditions' => array('token' => $token)));
			
			if($stoken){
				$this->Session->write('token', 'valido');
				$this->Session->write('token_id' , $token);
				$this->Email->id = $stoken['Email']['id'];
				$this->Email->saveField('abrio', 1);
				header("Location:".$this->webroot);
				exit();
			}
			else {
				header("Location:".$this->webroot);
				exit();
			}	
		}
		else {
			header("Location:".$this->webroot);
			exit();
		}
		
		
		
	}

	public function enviarInvitacion(){
		$this->autoRender = false;
		ini_set('max_execution_time', 300);
		$this->Session->write('token', 'valido');
		if($this->request->data['email']){
			

			require 'PHPMailer/PHPMailerAutoload.php';

			$mail = new PHPMailer;
			$email = $this->request->data['email'];
			$find = $this->Email->find('first' , array('conditions' => array('email' => $email)));
			
			if($find)
			{
				echo "Ya Enviaste a esta persona";
			}
			else
			{
				$data = array();
				$data['Email']['email'] = $this->request->data['email'];
				$data['Email']['nombre'] = $this->request->data['nombre'];
				$token = md5($this->request->data['email']);
				$data['Email']['token'] = $token;
				$this->Email->save($data);
			
			
			//$mail->SMTPDebug = 3;                               // Enable verbose debug output

			//$mail->isSMTP();                                      // Set mailer to use SMTP
			//$mail->Host = 'ssl://smtp.gmail.com';  // Specify main and backup SMTP servers
			//$mail->SMTPAuth = true;                               // Enable SMTP authentication
			//$mail->Username = 'manuperazafa@gmail.com';                 // SMTP username
			//$mail->Password = '?fasito123';                           // SMTP password
			//$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
			//$mail->Port = 465;                                    // TCP port to connect to

			$mail->setFrom('bodaandreyalfredo@gmail.com', 'bodaandreyalfredo');
			$mail->addAddress($this->request->data['email'], $this->request->data['nombre']);     // Add a recipient
			//$mail->addReplyTo('danielapinzon1990@gmail.com', 'Daniela Pinzon');
		

			$mail->isHTML(true);                                  // Set email format to HTML

			$mail->Subject = 'Boda Andre y Alfredo';
			$body    = 'Migdaly, Ana, Pablo Freddy y Luis Alfredo tienen el placer de informarles que sus hijos Andreina y Alfredo han decidido hacer una vida juntos. 
Queremos invitarles a que nos acompañen en este momento tan especial en el cual Dios unirá las familias Matheus Rangel  y Perdomo Seijas. <br> Invitacion valida para '.$this->request->data['personas'].' personas<br><br><br><img src="http://bodaandreyalfredo.com.ve/img/invitacion.jpg" width="640" height="480"/><br><br><br>Tenemos el gusto de invitarte a visitar este link, para que te enteres de todos los detalles de nuestra boda. <a href="http://bodaandreyalfredo.com.ve/EmailQueues/verificoToken/'.$token.'">http://bodaandreyalfredo.com.ve/</a> También podrás confirmar tu presencia a los eventos del inicio de nuestro Vivieron Felices para Siempre. <br> <br> Por favor confirmar antes del 01 de noviembre 2016.';
			//$mail->Body = '<img src="http://bodaandreyalfredo.com.ve/img/invitacion.jpg" width="640" height="480" />';
			$mail->Body = $body;
			$mail->isHTML(true);
			if(!$mail->send()) {
			    echo 'Message could not be sent.';
			    echo 'Mailer Error: ' . $mail->ErrorInfo;
			} else {
			    echo 'Mensaje Enviado';
			}

			}
		}


	}

	public function obtenerInvitaciones(){

		$this->autoRender = false;
		$data = array();
		$data = $this->Email->find('all');
		
		$arrayReturn =  array('data' => $data); 
		return json_encode($arrayReturn);
		//var_dump($data[0]);
		//return new CakeResponse(array('body' => json_encode($data)));
	}

	public function confirmoToken(){
		$this->autoRender = false;
		
		
		if($this->Session->read('token_id')){
			$stoken = $this->Email->find('first' , array('conditions' => array('token' => $this->Session->read('token_id'))));
			
			
			$this->Email->id = $stoken['Email']['id'];
			$this->Email->saveField('confirmo', 1);
				
				
		mail("andreinaperdomo@gmail.com","confirmacion","La persona ".$stoken['Email']['nombre']." , ".$stoken['Email']['email']." a confirmado");
			echo json_encode(1);
		} 

				
		
		
		
	}

	public function obtenerMensajes(){
		$this->autoRender = false;
		
		
		$data =$this->Mensaje->find('all');
		return json_encode($data);
				
		
		
		
	}

	public function guardarMensaje(){
		$this->autoRender = false;
		if($this->request->data){
			$nombre = $this->request->data['nombre'];
			$mensaje = $this->request->data['mensaje'];
			$data['Mensaje']['nombre'] = $nombre;
			$data['Mensaje']['mensaje'] = $mensaje;
			$this->Mensaje->save($data);
			return json_encode(1);
				
		}
		else {
			return json_encode(0);
		}
		

	}

	public function enviarCorreos(){
		$this->autoRender = false;
		
		$correo = $this->Email->find('first' ,  array('conditions' => array('is_send' => 0)));
		
		require 'PHPMailer/PHPMailerAutoload.php';

			$mail = new PHPMailer;
			$mail->setFrom('bodaandreyalfredo@gmail.com', 'bodaandreyalfredo');
			$mail->addAddress($correo['Email']['email'], $correo['Email']['nombre']);     // Add a recipient
			//$mail->addReplyTo('danielapinzon1990@gmail.com', 'Daniela Pinzon');
		

			$mail->isHTML(true);                                  // Set email format to HTML

			$mail->Subject = 'Boda Andre y Alfredo';
			$body    = 'Migdaly, Ana, Pablo Freddy y Luis Alfredo tienen el placer de informarles que sus hijos Andreina y Alfredo han decidido hacer una vida juntos. 
Queremos invitarles a que nos acompañen en este momento tan especial en el cual Dios unirá las familias Matheus Rangel  y Perdomo Seijas. <br> Invitacion valida para '.$correo['Email']['cantidad_p'].' personas<br><br><br><img src="http://bodaandreyalfredo.com.ve/img/invitacion.jpg" width="640" height="480"/><br><br><br>Tenemos el gusto de invitarte a visitar este link, para que te enteres de todos los detalles de nuestra boda. <a href="http://bodaandreyalfredo.com.ve/EmailQueues/verificoToken/'.$correo['Email']['token'].'"> http://bodaandreyalfredo.com.ve </a> También podrás confirmar tu presencia a los eventos del inicio de nuestro Vivieron Felices para Siempre. <br> <br> Por favor confirmar antes del 01 de noviembre 2016.';
			//$mail->Body = '<img src="http://bodaandreyalfredo.com.ve/img/invitacion.jpg" width="640" height="480" />';
			$mail->Body = $body;
			$mail->isHTML(true);
			
		
			if(!$mail->send()) {
			    echo 'Message could not be sent.';
			    echo 'Mailer Error: ' . $mail->ErrorInfo;
			    $this->Email->id = $correo['Email']['id'];
				$this->Email->saveField('is_send', 0);
			    header("Refresh:10");
			} else {
				$this->Email->id = $correo['Email']['id'];
				$this->Email->saveField('is_send', 1);
				echo 'Message has been sent a '.$correo['Email']['email'];
			    header("Refresh:10");
			    
			}
           
	}

}
